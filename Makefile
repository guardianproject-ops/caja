-include $(shell curl -sSL -o .build-harness "https://gitlab.com/snippets/1957473/raw"; echo .build-harness)


.PHONY: clean
dist-clean:
	rm -rf dist build caja.egg-info

.PHONY: build
build:
	python3 setup.py sdist bdist_wheel

.PHONY: deploy
deploy: dist-clean build
	python3 -m twine upload --repository gitlab dist/*
