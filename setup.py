import os
from setuptools import setup, find_packages


setup(
    name="caja",
    description="A service that authorizes CF Access requests for nginx",
    long_description=open("README.md").read(),
    version="0.0.4",
    author="Abel Luck",
    author_email="abel@guardianproject.info",
    url="https://gitlab.com/guardianproject-ops/caja",
    packages=find_packages(exclude=["ez_setup"], include=["caja", "caja.*"]),
    install_requires=open(
        os.path.join(os.path.dirname(__file__), "requirements.txt")
    ).readlines(),
    setup_requires=['flake8'],
    entry_points={"console_scripts": ["caja = caja.main:main"]},
    license="AGPL3",
)
