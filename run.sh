#! /usr/bin/env bash

set -e

if [ -f .env ]; then
  source .env
fi

HOST=${1:-localhost:8081}
WORKERS=${2:-1}

RUN_COMMAND="talisker.gunicorn caja.main:app --bind $HOST --worker-class sync --workers $WORKERS --name talisker-`hostname`"

if [ "${FLASK_DEBUG}" = true ] || [ "${FLASK_DEBUG}" = 1 ]; then
    RUN_COMMAND="${RUN_COMMAND} --reload --log-level debug --timeout 9999"
fi

${RUN_COMMAND}

