import json
import jwt
import os
import requests
import time
from caja.util import validate_cfg, get_cfg, is_devel_mode
from flask import request
from functools import lru_cache
from logging import getLogger
from talisker.flask import TaliskerApp
from werkzeug.debug import DebuggedApplication
from werkzeug.middleware.proxy_fix import ProxyFix

validate_cfg()
logger = getLogger(__name__)
app = TaliskerApp(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
if app.debug:
    app.wsgi_app = DebuggedApplication(app.wsgi_app)

PORT = get_cfg("CAJA_PORT", 5001)
BIND_ADDR = get_cfg("CAJA_BIND_ADDR", "127.0.0.1")
POLICY_AUD = get_cfg("CF_POLICY_AUD")
AUTH_DOMAIN = get_cfg("CF_AUTH_DOMAIN")
IDP = get_cfg("CF_IDENTITY_PROVIDER", "")
CERTS_URL = "https://{}/cdn-cgi/access/certs".format(AUTH_DOMAIN)
IDENTITY_URL = "https://{}/cdn-cgi/access/get-identity".format(AUTH_DOMAIN)
EXPORTED_USER_HEADER = get_cfg("CF_EXPORTED_USER_HEADER", "X-Forwarded-Principal")
EXPORTED_ROLES_HEADER = get_cfg("CF_EXPORTED_ROLES_HEADER", "X-Forwarded-Roles")


def _get_public_keys():
    """
    Returns:
        List of RSA public keys usable by PyJWT.
    """
    r = requests.get(CERTS_URL)
    public_keys = []
    jwk_set = r.json()
    for key_dict in jwk_set["keys"]:
        public_key = jwt.algorithms.RSAAlgorithm.from_jwk(json.dumps(key_dict))
        public_keys.append(public_key)
    return public_keys


def github_identity_parser(payload):
    resp = {"idp": "github", "groups": []}
    if "teams" in payload:
        teams = payload["teams"]
        for team in teams:
            resp["groups"].append({"name": team["name"], "id": team["name"]})
    if "name" in payload:
        resp["name"] = payload["name"]
    if "email" in payload:
        resp["email"] = payload["email"]
        resp["principal"] = payload["email"]
    if "uid" in payload:
        resp["uid"] = payload["uid"]
    if "idp" in payload:
        resp["idp_id"] = payload["idp"]["id"]

    return resp


parsers = {"github": github_identity_parser}


@lru_cache()
def get_identity(token, ttl_hash=None):
    """
    Given a CF token, fetches the identity from CF
    """
    if len(IDP) == 0:
        logger.debug("no idp configured")
        return None
    cookies = {"CF_Authorization": token}
    r = requests.get(IDENTITY_URL, cookies=cookies)
    payload = r.json()

    parser = parsers[IDP]
    if not parser:
        logger.debug("no idp parser avaialble")
        return None
    logger.debug("using idp parser %s", IDP)

    logger.debug("payload %s", payload)
    return parser(payload)


def rundeck_headers(settings):
    """
    Given a list of CF identity groups, returns a list
    of headers that can be used to login to rundeck
    """
    result = {}

    if "principal" in settings:
        result[EXPORTED_USER_HEADER] = settings["principal"]
        result[EXPORTED_ROLES_HEADER] = "user,admin,architect,deploy,build"
    return result


def ttl_hash(seconds=600):
    return round(time.time() / seconds)


@app.route("/")
def verifier():
    token = ""
    if "CF_Authorization" in request.cookies:
        token = request.cookies["CF_Authorization"]
    else:
        return "missing required cf authorization token", 403
    keys = _get_public_keys()

    # Loop through the keys since we can't pass the key set to the decoder
    valid_token = False
    for key in keys:
        try:
            # decode returns the claims that has the email when needed
            jwt.decode(token, key=key, audience=POLICY_AUD)
            valid_token = True
            break
        except Exception:
            pass
    if not valid_token:
        return "invalid token", 403

    idp_settings = get_identity(token, ttl_hash=ttl_hash())
    if idp_settings:
        logger.debug("idp_settings %s", idp_settings)
        headers = rundeck_headers(idp_settings)
        logger.debug("got some headers %s", headers)

    return "ok", 200, headers


def main():
    debug = os.getenv("DEBUG", "true").lower() == "true" or is_devel_mode()
    app.run(debug=debug, host=BIND_ADDR, port=PORT)


if __name__ == "__main__":
    main()
