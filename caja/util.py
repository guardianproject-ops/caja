import os


def check_rw_perms(path):
    if os.path.exists(path):
        if os.path.isfile(path):
            return os.access(path, os.W_OK)
        else:
            return False
    parent = os.path.dirname(path)
    return os.access(parent, os.W_OK)


def get_cfg(key, default=None):
    """Returns a value from the environment, first trying the passed key
    then trying the _FILE variant, reading the value from the file if it exists
    """
    try:
        return os.environ[key]
    except KeyError:
        try:
            with open(os.environ[key + "_FILE"], "r") as f:
                return f.read().strip()
        except (KeyError, OSError):
            return default


class ConfigurationError(Exception):
    pass


def validate_cfg():
    required_keys = [
        "CF_POLICY_AUD",
        "CF_AUTH_DOMAIN",
        # "REDIS_URL",
    ]
    missing = []
    for k in required_keys:
        v = get_cfg(k)
        if v is None:
            missing.append(k)

    if len(missing) > 0:
        raise ConfigurationError(
            "Required config values not found: {}s".format(", ".join(missing))
        )


def is_devel_mode():
    v = get_cfg("DEVEL", False)
    return v is True or v == "true" or v == "1"
